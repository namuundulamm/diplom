using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [HideInInspector]
    public Vector3 startPos { get { return start.position; } }
    [HideInInspector]
    public Vector3 endPos { get { return end.position; } }
    [SerializeField]
    private Transform start;
    [SerializeField]
    private Transform end;

}
