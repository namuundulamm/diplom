using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeText : MonoBehaviour
{
    public float time;
    public AnimationCurve ac;

    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponentInChildren<Text>();


        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        Vector3 startPos = transform.position;
        Vector3 targetPos = transform.position + Vector3.up;

        Color startColor = text.color;
        Color targetColor = startColor;
        targetColor.a = 0;
        for (float t = 0; t < time; t += Time.deltaTime)
        {
            transform.position = Vector3.Lerp(startPos, targetPos, ac.Evaluate(t / time));
            text.color = Color.Lerp(startColor, targetColor, ac.Evaluate(t / time));

            yield return null;
        }
    }
}
