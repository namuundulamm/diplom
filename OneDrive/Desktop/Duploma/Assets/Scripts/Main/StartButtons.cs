using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButtons : MonoBehaviour
{
    public GameObject male, female, start;
    public int gender;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    // void Update()
    // {

    // }
    public void GenderMale()
    {
        start.gameObject.SetActive(true);
        male.gameObject.SetActive(false);
        female.gameObject.SetActive(false);
        gender = 0;
        FindObjectOfType<Player>().isPressed = true;
    }
    public void GenderFemale()
    {
        start.gameObject.SetActive(true);
        male.gameObject.SetActive(false);
        female.gameObject.SetActive(false);
        gender = 1;
        FindObjectOfType<Player>().isPressed = true;
    }
}
