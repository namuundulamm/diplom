﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : Singleton<LevelSpawner>
{
    public List<Level> levels;
    public int levelLength;
    public Transform startPos;
    public void Init()
    {
        LoadLevel();
        // LeaderBoardData.SetDatas();
    }
    public void LoadLevel()
    {
        var tilePf = Resources.Load<Tile>("Tile");

        var collectibles = Resources.LoadAll<Collectible>("Collectibles");
        var obstacle = Resources.Load<GameObject>("Obstacle/Bus");
        Vector3 lastPos = startPos.position;
        for (int i = 0; i < levelLength; i++)
        {
            var spawnedTile = Instantiate(tilePf, lastPos - tilePf.startPos, Q.O);
            var rndCollectible = collectibles[Rnd.Rng(0, collectibles.Length)];
            Vector3 offset = Vector3.left * 1f * Rnd.Rng(-1, 2) + Vector3.up * 0.2f;

            Instantiate(obstacle, spawnedTile.transform.position + Vector3.left * 1f * Rnd.Rng(-1, 2), Q.O);

            for (int j = 0; j < 3; j++)
            {
                Instantiate(rndCollectible, spawnedTile.startPos + offset + Vector3.forward * j, Quaternion.Euler(0, Rnd.Ang, 0) * rndCollectible.transform.rotation);
            }
            lastPos = spawnedTile.endPos;
        }

        Instantiate(Resources.Load<Tile>("Haais"), lastPos - Resources.Load<Tile>("Haais").startPos, Q.O);
    }
    int GetLevelIdx()
    {
        int res = GameController.Level - 1;
        if (GameController.Level > levels.Count)
        {
            res = Data.LevelIdx.I();
            if (res < 0 || GameController.IsWin)
            {
                res = Rnd.Idx(levels.Count, res);
                Data.LevelIdx.Set(res);
            }
        }
        return res;
    }
}