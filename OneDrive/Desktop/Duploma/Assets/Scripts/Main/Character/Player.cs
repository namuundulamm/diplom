﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player : Character
{
    public Text scoreText;
    Animator animator;
    int state;
    StartButtons startButtons;
    private void Awake()
    {
        // animator = GetComponentInChildren<Animator>();
        startButtons = FindObjectOfType<StartButtons>();
    }
    void Start()
    {
        // print(Data.Best.F());
    }
    public bool isPressed;
    bool isStateChange;
    void Update()
    {
        if (isPressed)
        {
            transform.Child(startButtons.gender).gameObject.SetActive(true);
            animator = transform.Child(startButtons.gender).GetComponentInChildren<Animator>();
            animator.SetInteger("State", state);
        }
        if (A.IsPlaying)
        {
            if (!isStateChange)
            {
                state = 1;
                isStateChange = true;
            }
            // else
            // {

            // }
        }

        if (IsDown)
        {
            MouseButtonDown();
        }

        if (A.IsPlaying)
            scoreText.text = "" + Data.Score.F();
    }
    public void MouseButtonDown()
    {
        mp = MP;
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Collectible"))
        {
            Instantiate(Resources.Load<FadeText>("FadeUpText"), other.transform.position, Q.O).text.text = "" + other.Gc<Collectible>().score;
            Data.Score.Set(Data.Score.F() + other.Gc<Collectible>().score);
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Obstacle"))
        {
            // animator.SetInteger("State", 3);
            state = 3;
            A.GameOver();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            if (Data.Score.F() < 0)
            {
                // animator.SetTrigger("Lose");
                // animator.SetInteger("State", 3);
                state = 3;
                A.GameOver();
                // return;
            }
            else
            {
                float b = Data.Best.F();
                float s = Data.Score.F();
                if (s >= b)
                {
                    Data.Best.Set(s);
                }
                // animator.SetTrigger("Win");
                // animator.SetInteger("State", 2);
                state = 2;
                A.LevelCompleted();
            }

        }
    }
}