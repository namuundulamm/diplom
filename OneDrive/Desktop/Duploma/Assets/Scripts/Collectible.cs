using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public int score = 100;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "Collectible";
    }


    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0, 1, 0) * transform.rotation;
    }


}
