using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float forwardSpeed = 10f;
    public float sideScrollSpeed = 1.5f;
    Rigidbody rb;
    Vector3 mp; float shiftDest;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!A.IsPlaying) return;

        Vector3 targetPosition = rb.position + Vector3.forward * forwardSpeed * Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            mp = Input.mousePosition;
        }
        float shiftValue = 0.02f, mouseDistance = Vector3.Distance(mp, Input.mousePosition), dragDistance = Screen.width / 500f;
        if (Input.GetMouseButton(0))
        {
            if (mp.x > Input.mousePosition.x)
            {
                shiftValue *= -1;
            }
            if (mp.x < Input.mousePosition.x)
            {
                shiftValue *= 1;
            }

            if (Mathf.Abs(mp.x - Input.mousePosition.x) > 0)
                if (Vector3.Distance(mp, Input.mousePosition) > dragDistance)
                {
                    shiftDest += shiftValue * Mathf.Ceil(mouseDistance / dragDistance) * sideScrollSpeed;
                }


            mp = Input.mousePosition;
            shiftDest = Mathf.Clamp(shiftDest, -1.4f, 1.4f);

            targetPosition.x = shiftDest;
        }
        rb.rotation = Quaternion.LookRotation(targetPosition - rb.position);
        rb.MovePosition(Vector3.Lerp(rb.position, targetPosition, Time.fixedDeltaTime * 10));
    }
}
